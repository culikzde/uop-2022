#ifndef TREE_H
#define TREE_H

#include <QTreeWidget>
#include <Inventor/nodes/SoNode.h>

class MainWindow;

class Tree : public QTreeWidget
{
    // Q_OBJECT
public:
    Tree (QWidget * parent = nullptr);
    MainWindow * win;

protected:
    QStringList mimeTypes() const;
    Qt::DropActions supportedDropActions() const;
    bool dropMimeData (QTreeWidgetItem *parent, int index, const QMimeData *data, Qt::DropAction action);
};

class TreeItem : public QTreeWidgetItem
{
public:
    SoNode * node;

    TreeItem (QTreeWidgetItem * parent) :
        QTreeWidgetItem (parent),
        node (nullptr)
    {
    }

/*
private:
    SoNode * node;

public :
    SoNode * getNode () { return node; }

    void setNode (SoNode * param) { node = param; }

    TreeItem () :
        QTreeWidgetItem (),
        node (nullptr)
    {
    }
*/
};



#endif // TREE_H
