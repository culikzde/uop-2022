#include "tree.h"
#include "toolbutton.h"
#include "mainwindow.h"
#include <QMimeData>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoGroup.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoCylinder.h>

Tree::Tree (QWidget * parent) :
    QTreeWidget (parent),
    win (nullptr)
{
    setAcceptDrops (true); // <-- important
    // setDropIndicatorShown (true);
    // setDragDropMode(InternalMove);
}

/* ---------------------------------------------------------------------- */

QStringList Tree::mimeTypes() const // <-- importatnt
{
    return QStringList () << "application/x-color" << toolFormat;
}

Qt::DropActions Tree::supportedDropActions() const
{
   return Qt::MoveAction | Qt::CopyAction  | Qt::LinkAction;
}

bool Tree::dropMimeData (QTreeWidgetItem * target, int index, const QMimeData * data, Qt::DropAction action)
{
    bool result = false;
    TreeItem * item = dynamic_cast < TreeItem * > (target);
    if (item != nullptr)
    {
        SoNode * node = item->node;
        if (data->hasColor ())
        {
            QColor color = data->colorData().value <QColor> ();

            // #include <Inventor/nodes/SoMaterial.h>
            SoMaterial * material = dynamic_cast < SoMaterial * > (node);
            if (material != nullptr)
            {
               float r = color.red () / 255.0;
               float g = color.green () / 255.0;
               float b = color.blue () / 255.0;
               if (action == Qt::CopyAction) // Ctrl Mouse
                   material->specularColor.setValue (r, g, b);
               else if (action == Qt::LinkAction) // Ctrl Shift Mouse
                   material-> emissiveColor.setValue (r, g, b);
               else
                  material->diffuseColor.setValue (r, g, b);
               result = true;
            }
        }
        else if (data->hasFormat(toolFormat))
        {
            QString tool = data->data(toolFormat);
            SoNode * new_node = nullptr;
            if (tool == "rectangle")
            {
                SoCube * cube = new SoCube ();
                cube->height = 2;
                new_node = cube;
            }
            else if (tool == "ellipse")
            {
                SoSphere * sphere = new SoSphere ();
                sphere->radius = 2;
                new_node = sphere;
            }
            else if (tool == "line")
            {
                SoCylinder * cylinder = new SoCylinder ();
                cylinder->height = 2;
                new_node = cylinder;
            }

            SoGroup * group = dynamic_cast <SoGroup *>(node);
            if (group != nullptr and new_node != nullptr)
            {
               group->addChild (new_node);
               if (win != nullptr)
               {
                   win->displayBranch (item, new_node);
               }
               result = true;
            }
        }
    }
    return result;
}
