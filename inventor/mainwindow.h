#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include "tree.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_tree_itemActivated(QTreeWidgetItem *item, int column);

public:
    void displayBranch (QTreeWidgetItem * target, SoNode * node);
private:
    void displayProperties (SoNode * node);
    void displayLine (QString name, QString Value);

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
