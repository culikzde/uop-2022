QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

SOURCES += \
    colorbutton.cc \
    toolbutton.cc \
    mainwindow.cpp \
    tree.cc

HEADERS += \
    colorbutton.h \
    toolbutton.h \
    mainwindow.h \
    tree.h

FORMS += \
    mainwindow.ui

RESOURCES += \
   resources.qrc

CONFIG += link_pkgconfig
PKGCONFIG += SoQt
LIBS += -lSoQt -lCoin

# Ubuntu 20.04, 22.04
# LIBS += -lSoQt -lCoin

# QMAKE_CXXFLAGS += $$(CXXFLAGS)
# QMAKE_CFLAGS += $$(CFLAGS)
# QMAKE_LFLAGS += -lSoQt

# dnf install SoQt-devel
# apt install libsoqt520-dev

