#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "colorbutton.h"
#include "toolbutton.h"
#include <QApplication>

#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoCone.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoMaterial.h>

#include <Inventor/engines/SoElapsedTime.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->hsplitter->setStretchFactor (0, 1);
    ui->hsplitter->setStretchFactor (1, 5);
    ui->hsplitter->setStretchFactor (2, 1);

    ui->vsplitter->setStretchFactor (0, 5);
    ui->vsplitter->setStretchFactor (1, 1);

    ui->tabWidget->clear ();
    QToolBar * colorbar = new QToolBar;
    ui->tabWidget->addTab (colorbar, "Colors");
    addColorButtons (colorbar);
    QToolBar * toolbar = new QToolBar;
    ui->tabWidget->addTab (toolbar, "Tools");
    addToolButtons (toolbar);

    SoQtExaminerViewer * examiner = new SoQtExaminerViewer (ui->widget);

    SoSeparator * root = new SoSeparator;

    // cone
    SoSeparator * group1 = new SoSeparator;

    SoTransform * shift1 = new SoTransform;

    shift1->translation.setValue (-2.0, 0.0, 0.0);
    group1->addChild (shift1);

    SoCube * block = new SoCube ();
    block->height = 0.2;

    group1->addChild (block);


    SoMaterial * redMaterial = new SoMaterial;
    redMaterial->diffuseColor.setValue (1.0, 0.0, 0.0);
    //redMaterial->transparency = 0.5;
    group1->addChild (redMaterial);

    SoCone * cone = new SoCone ();
    group1->addChild (cone);

    // #include <Inventor/engines/SoElapsedTime.h>
    SoElapsedTime *  timer = new SoElapsedTime;
    // block->height.connectFrom (& timer->timeOut);
    // cone->bottomRadius.connectFrom (& timer->timeOut);
    // cone->height.connectFrom (& timer->timeOut);

    root->addChild (group1);

    // sphere
    SoSeparator * group2 = new SoSeparator;

    SoMaterial * greenMaterial = new SoMaterial;
    greenMaterial->diffuseColor.setValue (0.0, 1.0, 0.0);
    group2->addChild (greenMaterial);

    SoSphere * sphere = new SoSphere ();
    group2->addChild (sphere);

    root->addChild (group2);

    // cube
    SoSeparator * group3 = new SoSeparator;

    SoTransform * shift3 = new SoTransform ();
    shift3->translation.setValue (1.6, 0.0, 1.6);
    shift3->scaleFactor.setValue (0.7, 0.7, 0.7);
    group3->addChild (shift3);

    SoMaterial * blueMaterial = new SoMaterial;
    blueMaterial->diffuseColor.setValue (0.0, 0.0, 1.0);
    group3->addChild (blueMaterial);

    SoCube * cube = new SoCube ();
    group3->addChild (cube);

    root->addChild (group3);

    examiner->setSceneGraph (root);

    ui->tree->win = this;
    ui->tree->header()->setVisible(false);
    displayBranch (ui->tree->invisibleRootItem(), root);
}

void MainWindow::displayBranch (QTreeWidgetItem * target, SoNode * node)
{
   TreeItem * item = new TreeItem (target);
   item->node = node;

   SoType type = node->getTypeId ();
   QString name = type.getName().getString();
   item->setText (0, name);

   SoGroup * group = dynamic_cast <SoGroup *> (node);
   if (group != nullptr)
   {
       int count = group->getNumChildren ();
       for (int i = 0; i < count; i++)
       {
           SoNode * sub_node = group->getChild (i);
           displayBranch (item, sub_node);
       }
   }
}

void MainWindow::on_tree_itemActivated (QTreeWidgetItem * item0, int column)
{
    TreeItem * item = dynamic_cast <TreeItem *> (item0);
    if (item != nullptr)
    {
        displayProperties (item->node);
    }
}


/* ---------------------------------------------------------------------- */

void MainWindow::displayProperties (SoNode * node)
{
    ui->prop->clear();
    ui->prop->setColumnCount(2);
    ui->prop->setRowCount(0);
    // QStringList labels;
    // labels << "Name";
    // labels << "Value";
    // ui->prop->setHorizontalHeaderLabels(labels);
    ui->prop->setHorizontalHeaderLabels (QStringList () << "Name" << "Value");

    // displayLine ("abc", "def");

    SoFieldList list;
    node->getFields (list);

    int cnt = list.getLength();
    for (int i = 0; i < cnt; i++)
    {
        SoField * field = list [i];

        SbName name;
        node->getFieldName (field, name);

        SbString value;
        field->get (value);

        displayLine (name.getString(), value.getString ());
    }
}

void MainWindow::displayLine (QString name, QString value)
{
    int line = ui->prop->rowCount ();
    ui->prop->setRowCount (line+1);

    QTableWidgetItem * item = new QTableWidgetItem;
    item->setText (name);
    ui->prop->setItem (line, 0, item);

    item = new QTableWidgetItem;
    item->setText (value);
    ui->prop->setItem (line, 1, item);
}

MainWindow::~MainWindow()
{
    delete ui;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SoQt::init ((QWidget*) NULL); /* doplneno*/
    MainWindow w;
    w.show();
    return a.exec();
}


