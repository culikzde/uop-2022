#ifndef TOOLBUTTON_H
#define TOOLBUTTON_H

#include <QToolButton>
#include <QToolBar>

#include <QMimeData>
#include <QDrag>
#include <QMouseEvent>

const QString toolFormat = "application/x-tool";

class ToolButton : public QToolButton
{
public:
    ToolButton();
private:
   QString name;
   QIcon icon;
   // QPixmap getPixmap ();
protected:
    void mousePressEvent (QMouseEvent *event);
public:
    ToolButton (QString p_name, QString p_icon_name = "");
};

void addToolButtons (QToolBar * page);

#endif // TOOLBUTTON_H
