path C:\Program Files\Git\usr\bin

set PC=gp5

ssh %PC% mkdir -p temp/test
ssh %PC% rm temp/test/cuda-mini.bin

REM ssh %PC%
REM ssh %PC% -t mc

scp cuda-mini.cu %PC%:temp/test/cuda-mini.cu

REM ssh %PC% "cd temp/test ; /opt/cuda/bin/nvcc cuda-mini.cu -o cuda-mini.bin"
ssh %PC% "cd temp/test ; /opt/cuda/bin/nvcc cuda-mini.cu -o cuda-mini.bin -gencode arch=compute_35,code=sm_35"

ssh %PC% "ls -l temp/test"
ssh %PC% "cd temp/test ; ./cuda-mini.bin"

pause