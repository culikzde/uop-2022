PC="gp5"

ssh $PC mkdir -p temp/test
ssh $PC "test -f temp/test/cuda-mini.bin && rm temp/test/cuda-mini.bin"

scp cuda-mini.cu $PC:temp/test/cuda-mini.cu

# ssh $PC "cd temp/test ; /opt/cuda/bin/nvcc cuda-mini.cu -o cuda-mini.bin"
ssh $PC "cd temp/test ; /opt/cuda/bin/nvcc cuda-mini.cu -o cuda-mini.bin -gencode arch=compute_35,code=sm_35"

ssh $PC "ls -l temp/test"
ssh $PC "cd temp/test ; ./cuda-mini.bin"
