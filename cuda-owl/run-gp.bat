path C:\Program Files\Git\usr\bin

set PC=gp5

set CODE=35

ssh %PC% mkdir -p temp/test

scp Heat_Equation_2D_Jacobi.cu %PC%:temp/test

ssh %PC% "cd temp/test && /opt/cuda/bin/nvcc Heat_Equation_2D_Jacobi.cu -o heat.bin  -gencode arch=compute_%CODE%,code=sm_%CODE% -l cublas"
REM ssh %PC% "cd temp/test && /opt/cuda/bin/nvcc Heat_Equation_2D_Jacobi.cu -o heat.bin -l cublas"

ssh %PC% "cd temp/test && ./heat.bin"

ssh %PC% "ls -l temp/test"

REM ssh %PC%
REM ssh %PC% -t "cd temp/test && mc"

pause