path C:\Program Files\Git\usr\bin

set PC=helios

ssh %PC% mkdir -p temp/test
REM ssh %PC% rm temp/test/cuda-mini.bin

scp Heat_Equation_2D_Jacobi.cu %PC%:temp/test
scp makefile %PC%:temp/test
scp run.cfg %PC%:temp/test

ssh %PC% "cd temp/test && module load cuda/10.2 && make"

REM ssh %PC% "cd temp/test && module load cuda/10.2 && qsub -q gpu -j oe -N Pokus_CUDA -l walltime=01:00:00 -l select=1:mem=4G:ncpus=4:ngpus=1 -- ./heat.bin"
ssh %PC% "cd temp/test && module load cuda/10.2 && qsub run.cfg"

ssh %PC% "qstat -q gpu"
ssh %PC% "ls -l temp/test"
ssh %PC% "cat temp/test/new.txt"

REM ssh %PC%
ssh %PC% -t "cd temp/test && module load cuda/10.2 && mc"

pause